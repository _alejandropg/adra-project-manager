<?php

/*
|--------------------------------------------------------------------------
| Instalador del módulo
|--------------------------------------------------------------------------
|
| Aquí se establecerán todas las reglas de instalación
| de tal forma que se pueda instalar automaticamente el módulo
| sin necesidad de instalarlo manualmente, ejecutar migraciones, 
| copiar assets, etc.
|
*/

if( sys_installed() ) {

	module_install(__DIR__, function( $module ) {
		
		$roles = [
            [
            	'name' => [
                    'en' => 'Employee', 
                    'es' => 'Empleado',
                    'pt' => 'Empregado'
                ],
            	'route' => '/admin/agencies'
            ],
            [
            	'name' => [
                    'en' => 'Voluntary', 
                    'es' => 'Voluntario',
                    'pt' => 'Voluntário'
                ],
            	'route' => '/admin/agencies'
            ],
        ];

        foreach($roles as $role) {
            $rol = roles()->create($role);
        }    

	});
}